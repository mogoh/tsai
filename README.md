# Tsai - a discord bot

## ToDo

- update libraries

### Ideas

- markov: improve markov chain library
- olid: send image direct.
- roles by emoji-recation
- dice
- oracle
- rekt
- Inspirational Pictures
- rss-feed
- twitch-feed
  - configure streams to display manualy
- music
- soundboard
- generate a tiny sprite sheet https://codepen.io/KilledByAPixel/pen/ZEWyRwO?editors=0010
- make fancy output

## Links

- https://discordjs.guide/
- https://discord.js.org/
- https://discord.com/developers/applications
- https://github.com/twurple/twurple
- https://twurple.js.org/

## Run without Docker

- Configure Tsai:
  - `cp data/settings.json.example data/settings.json`
  - `editor data/settings.json`

```
yarn install --immutable
yarn tsc
node  ./dist/main.js
```

## Install and Run with Docker

- Install Docker
- Configure Docker enviroment:
  - `cp .env.example .env`
  - `editor .env`
- Configure Tsai:
  - `cp data/settings.json.example data/settings.json`
  - `editor data/settings.json`
- `. .env`
- `docker compose build`
- `docker compose up --detach`

```
. .env && docker compose build && docker compose up --detach
```
